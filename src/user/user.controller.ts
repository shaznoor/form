import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async getAllUser() {
    return await this.userService.getAllUser();
  }

  @Get(':id')
  async getUser(@Param('id') userId) {
    return await this.userService.getUser(userId);
  }

  @Delete(':id')
  async deleteUser(@Param('id') userId) {
    return await this.userService.deleteUser(userId);
  }

  @Put()
  async editUser(
    @Body('id') userId: string,
    @Body('name') userName: string,
    @Body('email') userEmail: string,
    @Body('mobile') userMobile: string,
    @Body('gender') userGender: string,
    @Body('tech') userTech: string,
  ) {
    return await this.userService.editUser(
      userId,
      userName,
      userEmail,
      userMobile,
      userGender,
      userTech,
    );
  }

  @Post()
  async addUser(
    @Body('name') userName: string,
    @Body('email') userEmail: string,
    @Body('mobile') userMobile: string,
    @Body('gender') userGender: string,
    @Body('tech') userTech: string,
  ) {
    return await this.userService.insertUser(
      userName,
      userEmail,
      userMobile,
      userGender,
      userTech,
    );
  }
}
