import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  mobile: { type: String, required: true },
  gender: { type: String, required: true },
  tech: { type: String, required: true },
});

export interface User extends Document {
  id: string;
  name: string;
  email: string;
  mobile: string;
  gender: string;
  tech: string;
}
