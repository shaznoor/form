import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;

  const mockUserService = {
    addUser: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
    })
      .overrideProvider(UserService)
      .useValue(mockUserService)
      .compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should add new user', () => {
    expect(
      controller.addUser({
        name: 'Md Shaz Hassan',
        email: 'shaznoor9@gmail.com',
        mobile: '7903719147',
        gender: 'Male',
        tech: 'C++,Java',
      }),
    ).toEqual({
      id: expect.any(Number),
      name: 'Md Shaz Hassan',
      email: 'shaznoor9@gmail.com',
      mobile: '7903719147',
      gender: 'Male',
      tech: 'C++,Java',
    });
  });
});
