import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './user.model';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async getAllUser() {
    const users = await this.userModel.find().exec();
    return users;
  }

  async getUser(userId: string) {
    const user = await this.userModel.findById(userId);
    return user;
  }

  async deleteUser(userId: string) {
    await this.userModel.deleteOne({ _id: userId }).exec();
    return null;
  }

  async editUser(
    userId: string,
    userName: string,
    userEmail: string,
    userMobile: string,
    userGender: string,
    userTech: string,
  ) {
    const getSingleUser = await this.getUser(userId);
    getSingleUser.name=userName;
    getSingleUser.email=userEmail;
    getSingleUser.mobile=userMobile;
    getSingleUser.gender=userGender;
    getSingleUser.tech=userTech;
    const res=await getSingleUser.save();
    return res;
  }

  async insertUser(
    name: string,
    email: string,
    mobile: string,
    gender: string,
    tech: string,
  ) {
    const newUser = new this.userModel({
      name: name,
      email: email,
      mobile: mobile,
      gender: gender,
      tech: tech,
    });
    const userId = await newUser.save();
    return userId.id;
  }
}
